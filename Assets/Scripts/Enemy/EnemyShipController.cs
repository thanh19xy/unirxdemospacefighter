using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public class EnemyShipController : ShipController
    {
        public ShipController Target { get; private set; }

        public EnemyStateManager StateManager { get; private set; }

        private void Awake()
        {
            this.Target = GameObject.FindWithTag(Contants.PLAYER_TAG).GetComponent<ShipController>();
            this.StateManager = this.GetComponent<EnemyStateManager>();
        }
    }
}