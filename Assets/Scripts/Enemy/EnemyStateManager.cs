﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Serialization;

namespace UniRxDemo.SpaceFighter
{
    public enum EnemyState
    {
        Idle,
        Attack,
        Seek,
        None,
    }

    [RequireComponent(typeof(EnemyShipController))]
    public class EnemyStateManager : MonoBehaviour
    {
        private EnemyShipController ShipController { get; set; }

        [SerializeField] private Settings settings;

        private Settings CommonSetting => settings;

        private ReactiveProperty<EnemyState> EnemyStateRp { get; } = new ReactiveProperty<EnemyState>(EnemyState.None);

        public IObservable<EnemyState> OnChangedStateAsObservable() => EnemyStateRp.AsObservable();

        private void Awake()
        {
            this.ShipController = this.GetComponent<EnemyShipController>();
        }

        private void Start()
        {
            this.OnDisableAsObservable()
                .Subscribe(_ => this.EnemyStateRp.Value = EnemyState.None);
        }

        private void OnEnable()
        {
            Observable.EveryUpdate()
                .TakeUntilDisable(this)
                .Subscribe(_ => this.EnemyStateRp.Value = this.SwitchState());
        }

        private EnemyState SwitchState()
        {
            if (ShipController.Target.IsDied)
            {
                return EnemyState.Idle;
            }
            else if (CanAttack())
            {
                return EnemyState.Attack;
            }
            else
            {
                return EnemyState.Seek;
            }
        }

        private bool CanAttack()
        {
            return (ShipController.Target.Position - ShipController.Position).magnitude < CommonSetting.AttackRangeBuffer;
        }

        [Serializable]
        public class Settings
        {
            [SerializeField] private float attackRangeBuffer;

            public float AttackRangeBuffer => attackRangeBuffer;
        }
    }
}