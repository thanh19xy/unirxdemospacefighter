using System;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UniRxDemo.SpaceFighter
{
    public class EnemyStateIdle : EnemyStateBase
    {
        [SerializeField] private Settings settingState;

        private Settings SettingState => settingState;

        private float Theta { get; set; }

        protected override EnemyState IdState => EnemyState.Idle;

        protected override void Start()
        {
            base.Start();

            Theta = Random.Range(0, 2.0f * Mathf.PI);

            this.OnEnterStateSafeWith(Observable.EveryFixedUpdate())
                .Subscribe(_ => Idle());
        }

        private void Idle()
        {
            Theta += Time.deltaTime * SettingState.Frequency;

            ShipController.Position = ShipController.Position + ShipController.RightDir * SettingState.Amplitude * Mathf.Sin(Theta);
        }

        [Serializable]
        public class Settings
        {
            [SerializeField] private float amplitude;

            public float Amplitude => amplitude;

            [SerializeField] private float frequency;

            public float Frequency => frequency;
        }
    }
}