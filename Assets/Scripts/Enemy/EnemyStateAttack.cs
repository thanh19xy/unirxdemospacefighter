using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UniRxDemo.SpaceFighter
{
    [RequireComponent(typeof(AudioPlayer))]
    public class EnemyStateAttack : EnemyStateBase, IPoolable<Bullet>
    {
        [SerializeField] private Settings settings;

        private Settings SettingState => settings;

        protected override EnemyState IdState => EnemyState.Attack;

        private AudioPlayer AudioPlayer { get; set; }

        private bool StrafeRight { get; set; }

        protected override void Awake()
        {
            base.Awake();
            this.AudioPlayer = this.GetComponent<AudioPlayer>();
        }

        protected override void Start()
        {
            base.Start();
            this.EnableFire();
            this.StrafeRight = Random.Range(0.0f, 1.0f) < 0.5f;
        }

        private void EnableFire()
        {
            var shootIntervalStream = Observable.EveryUpdate()
                .ThrottleFirst(TimeSpan.FromSeconds(SettingState.ShootInterval));

            this.OnEnterStateSafeWith(shootIntervalStream)
                .Subscribe(_ => Fire());

            this.OnEnterStateSafeWith(Observable.Timer(TimeSpan.FromSeconds(SettingState.StrafeChangeInterval)))
                .Subscribe(_ => this.StrafeRight = !StrafeRight);

            this.OnEnterStateSafeWith(Observable.EveryFixedUpdate())
                .Subscribe(_ => Strafe());
        }

        private void Fire()
        {
            this.RentAsync()
                .Subscribe(bullet =>
                {
                    bullet.OnSpawned(this.SettingBullet());

                    bullet.transform.position =
                        ShipController.Position + ShipController.LookDir * SettingState.BulletOffsetDistance;
                    bullet.transform.rotation = ShipController.Rotation;
                });

            this.AudioPlayer.Play(SettingState.ShootSound, SettingState.ShootSoundVolume);
        }

        private SettingBullet SettingBullet()
        {
            return new SettingBullet(SettingState.BulletSpeed,
                SettingState.BulletLifetime,
                SettingState.MaterialBullet,
                this.gameObject.layer);
        }

        // Strafe to avoid getting hit too easily
        private void Strafe()
        {
            var force = ((StrafeRight) ? ShipController.RightDir : -ShipController.RightDir) * SettingState.StrafeMultiplier;
            ShipController.AddForce(force);
        }

        [Serializable]
        public class Settings
        {
            public AudioClip ShootSound => shootSound;

            public float ShootSoundVolume => shootSoundVolume;

            public float BulletLifetime => bulletLifetime;

            public float BulletSpeed => bulletSpeed;

            public float BulletOffsetDistance => bulletOffsetDistance;

            public float ShootInterval => shootInterval;

            public Material MaterialBullet => materialBullet;

            public float ErrorRangeTheta => errorRangeTheta;

            public float StrafeMultiplier => strafeMultiplier;

            public float StrafeChangeInterval => strafeChangeInterval;


            [SerializeField] private AudioClip shootSound;

            [SerializeField] private float bulletSpeed;

            [SerializeField] private float shootSoundVolume = 1.0f;

            [SerializeField] private float bulletLifetime;

            [SerializeField] private float bulletOffsetDistance;

            [SerializeField] private float shootInterval;

            [SerializeField] private Material materialBullet;

            [SerializeField] private float errorRangeTheta;

            [SerializeField] private float strafeMultiplier;

            [SerializeField] private float strafeChangeInterval;
        }
    }
}