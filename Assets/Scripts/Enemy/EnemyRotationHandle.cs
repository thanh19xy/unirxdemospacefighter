using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public class EnemyRotationHandle : MonoBehaviour
    {
        [SerializeField] private Settings settings;

        private Settings Setting => settings;

        private EnemyShipController ShipController { get; set; }

        private Vector3 DesiredLookDir()
        {
            return (ShipController.Target.Position - ShipController.Position).normalized;
        }

        private void Awake()
        {
            this.ShipController = this.GetComponent<EnemyShipController>();
        }

        private void OnEnable()
        {
            Observable.EveryFixedUpdate()
                .TakeUntilDisable(this)
                .Subscribe(_ => Rotation());
        }

        private void Rotation()
        {
            var lookDir = ShipController.LookDir;

            var error = Vector3.Angle(lookDir, DesiredLookDir());

            if (Vector3.Cross(lookDir, DesiredLookDir()).z < 0)
            {
                error *= -1;
            }

            ShipController.AddTorque(error * Setting.TurnSpeed);
        }

        [Serializable]
        public class Settings
        {
            [SerializeField] private float turnSpeed;

            public float TurnSpeed => turnSpeed;
        }
    }
}