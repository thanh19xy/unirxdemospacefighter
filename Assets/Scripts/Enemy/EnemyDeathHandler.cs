using System;
using UnityEngine;
using UniRx;

namespace UniRxDemo.SpaceFighter
{
    [RequireComponent(typeof(AudioPlayer))]
    public class EnemyDeathHandler : MonoBehaviour,
        IReceiveDamage,
        IPoolable<Explosion>,
        IPoolable<EnemyShipController>
    {
        [SerializeField] private Settings settings;

        private Settings Setting => this.settings;

        private AudioPlayer AudioPlayer { get; set; }

        private EnemyShipController ShipController { get; set; }

        private void Awake()
        {
            this.AudioPlayer = this.GetComponent<AudioPlayer>();
            this.ShipController = this.GetComponent<EnemyShipController>();
        }

        private void Start()
        {
            this.ShipController.OnDiedAsObservable
                .Subscribe(_ => this.Die());
        }

        private void Die()
        {
            this.AudioPlayer.Play(Setting.DeathSound, Setting.DeathSoundVolume);

            this.RentAsync<Explosion>()
                .Subscribe(explosion =>
                {
                    explosion.transform.position = ShipController.Position;
                    explosion.OnSpawned();
                });

            this.ReturnPool<EnemyShipController>(this.ShipController);
        }
        
        public void TakeDamage(DamageMessage damageMessage)
        {
            this.ShipController.TakeDamage(damageMessage);
        }

        [Serializable]
        public class Settings
        {
            [SerializeField] private AudioClip deathSound;

            public AudioClip DeathSound => deathSound;

            [SerializeField] private float deathSoundVolume = 1.0f;

            public float DeathSoundVolume => deathSoundVolume;
        }
    }
}