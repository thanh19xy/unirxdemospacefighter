using System;
using UniRx;
using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    [RequireComponent(typeof(ShipController))]
    public abstract class EnemyStateBase : MonoBehaviour, IEnemyState
    {
        private Subject<Unit> OnEnterStateSubject { get; set; } = new Subject<Unit>();

        private Subject<Unit> OnExitStateSubject { get; set; } = new Subject<Unit>();

        protected EnemyShipController ShipController { get; set; }

        protected abstract EnemyState IdState { get; }

        protected virtual void Awake()
        {
            this.ShipController = GetComponent<EnemyShipController>();
        }

        protected virtual void Start()
        {
            this.ShipController.StateManager.OnChangedStateAsObservable()
                .Where(state => state == IdState)
                .Subscribe(_ => this.EnterState())
                .AddTo(this);

            this.ShipController.StateManager.OnChangedStateAsObservable()
                .Where(state => state != IdState)
                .Subscribe(_ => this.ExitState())
                .AddTo(this);
        }

        public void EnterState()
        {
            this.OnEnterStateSubject.OnNext(Unit.Default);
        }

        public IObservable<Unit> OnEnterStateAsObservable() =>
            this.OnEnterStateSubject ?? (OnEnterStateSubject = new Subject<Unit>());

        public void ExitState()
        {
            this.OnExitStateSubject.OnNext(Unit.Default);
        }

        public IObservable<Unit> OnExitStateAsObservable() =>
            this.OnExitStateSubject ?? (OnExitStateSubject = new Subject<Unit>());

        protected IObservable<TR> OnEnterStateSafeWith<TR>(IObservable<TR> other)
        {
            return OnEnterStateAsObservable()
                .SelectMany(other)
                .TakeUntilDisable(this)
                .TakeUntil(OnExitStateAsObservable())
                .RepeatUntilDestroy(this)
                .AsObservable();
        }

        protected virtual void RaiseOnCompletedOnDestroy()
        {
            OnEnterStateSubject?.OnCompleted();
            OnExitStateSubject?.OnCompleted();
        }

        private void OnDestroy()
        {
            this.RaiseOnCompletedOnDestroy();
        }
    }
}