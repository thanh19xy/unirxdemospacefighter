using System;
using UniRx;
using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public class EnemyStateSeek : EnemyStateBase
    {
        [SerializeField] private float seekCoffiency = 1f;

        private float SeekCoffiency => seekCoffiency;

        private Vector3 SteeringForce { get; set; } = Vector3.zero;

        protected override EnemyState IdState => EnemyState.Seek;

        protected override void Start()
        {
            base.Start();

            this.OnEnterStateSafeWith(Observable.EveryFixedUpdate())
                .Subscribe(_ => { this.ShipController.AddForce(Seek(ShipController.Target.Position)); });
        }

        private Vector3 Seek(Vector3 target)
        {
            var desiredVelocity = (target - transform.position).normalized * SeekCoffiency;
            var seek = desiredVelocity - ShipController.Velocity;
            return seek;
        }
    }
}