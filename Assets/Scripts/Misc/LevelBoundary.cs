using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public class LevelBoundary
    {
        private Camera Camera { get; } = Camera.main;

        public float Bottom => -ExtentHeight;

        public float Top => ExtentHeight;

        public float Left => -ExtentWidth;

        public float Right => ExtentWidth;

        public float ExtentHeight => Camera.orthographicSize;

        public float Height => ExtentHeight * 2.0f;

        public float ExtentWidth => Camera.aspect * Camera.orthographicSize;

        public float Width => ExtentWidth * 2.0f;
    }
}