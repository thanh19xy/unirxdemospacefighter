using System;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

namespace UniRxDemo.SpaceFighter
{
    public interface IInputHandler
    {
    }

    public static class InputExtension
    {
        public static IObservable<Unit> OnInputKeyAsObservable(this IInputHandler inputHandler, KeyCode keyCode)
        {
            var component = (Component) inputHandler;
            return component.OnKeyDownAsObservable(keyCode)
                .SelectMany(_ => component.UpdateAsObservable())
                .TakeUntil(component.OnKeyUpAsObservable(keyCode))
                .RepeatUntilDestroy(component);
        }
        
        public static IObservable<Unit> OnInputMouseAsObservable(this IInputHandler inputHandler, MouseButton mouseButton)
        {
            var component = (Component) inputHandler;
            return component.UpdateAsObservable()
                .Where(_ => Input.GetMouseButtonDown(mouseButton.GetHashCode()));
        }
    }
}