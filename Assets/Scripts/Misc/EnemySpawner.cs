using System;
using UnityEngine;
using UniRx;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

namespace UniRxDemo.SpaceFighter
{
    public class EnemySpawner : MonoBehaviour, IPoolable<EnemyShipController>
    {
        [SerializeField] private Settings settings;

        private Settings Setting => settings;

        private LevelBoundary LevelBoundary { get; set; }

        private int EnemyCount { get; set; }

        private float DesiredNumEnemies { get; set; }

        private ReactiveProperty<int> KillCount { get; } = new ReactiveProperty<int>(0);

        public IObservable<int> KillCountAsObservable() => this.KillCount.AsObservable();

        private void Start()
        {
            this.Initialize();

            Observable.EveryUpdate()
                .Select(_ => Setting.NumEnemiesIncreaseRate * Time.deltaTime)
                .Scan((previous, current) => previous + current)
                .Select(increaseRate => (int) (this.DesiredNumEnemies += increaseRate))
                .Select(rate => Mathf.Min(rate, Setting.MaxEnemy))
                .Where(rate => EnemyCount < rate)
                .Subscribe(_ =>
                {
                    SpawnEnemy();
                    EnemyCount++;
                }).AddTo(this);
        }

        private void Initialize()
        {
            this.LevelBoundary = new LevelBoundary();
            this.DesiredNumEnemies = Setting.NumEnemiesStartAmount;
        }

        private void SpawnEnemy()
        {
            this.RentAsync()
                .Subscribe(enemy =>
                {
                    enemy.Position = this.ChooseRandomStartPosition();
                    enemy.OnRespawn(Setting.HealthEnemyOnSpawn);
                    enemy.OnDiedAsObservable
                        .First()
                        .Subscribe(_ => OnEnemyKilled());
                });
        }

        private void OnEnemyKilled()
        {
            EnemyCount--;
            KillCount.Value++;
        }

        private Vector3 ChooseRandomStartPosition()
        {
            var side = Random.Range(0, 4);
            var posOnSide = Random.Range(0, 1.0f);

            float buffer = 2.0f;

            switch (side)
            {
                case 0:
                    // top
                {
                    return new Vector3(
                        LevelBoundary.Left + posOnSide * LevelBoundary.Width,
                        LevelBoundary.Top + buffer, 0);
                }
                case 1:
                    // right
                {
                    return new Vector3(
                        LevelBoundary.Right + buffer,
                        LevelBoundary.Top - posOnSide * LevelBoundary.Height, 0);
                }
                case 2:
                    // bottom
                {
                    return new Vector3(
                        LevelBoundary.Left + posOnSide * LevelBoundary.Width,
                        LevelBoundary.Bottom - buffer, 0);
                }
                case 3:
                    // left
                {
                    return new Vector3(
                        LevelBoundary.Left - buffer,
                        LevelBoundary.Top - posOnSide * LevelBoundary.Height, 0);
                }
            }

            throw new Exception("Not return vector3");
        }

        [Serializable]
        public class Settings
        {
            [SerializeField] private float healthEnemyOnSpawn;

            public float HealthEnemyOnSpawn => healthEnemyOnSpawn;

            [SerializeField] private float numEnemiesIncreaseRate;

            public float NumEnemiesIncreaseRate => numEnemiesIncreaseRate;

            [SerializeField] private float numEnemiesStartAmount;

            public float NumEnemiesStartAmount => numEnemiesStartAmount;

            [SerializeField] private float minDelayBetweenSpawns = 0.5f;

            public float MinDelayBetweenSpawns => minDelayBetweenSpawns;

            [SerializeField] private int maxEnemy = 5;

            public int MaxEnemy => maxEnemy;
        }
    }
}