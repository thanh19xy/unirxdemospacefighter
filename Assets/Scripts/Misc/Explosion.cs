using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public class Explosion : MonoBehaviour, IPoolable<Explosion>
    {
        [SerializeField] private float lifeTime;

        private float LifeTime => lifeTime;

        [SerializeField] private ParticleSystem particleSystem;

        private ParticleSystem ParticleSystem => particleSystem;

        private void Init()
        {
            Observable.Timer(TimeSpan.FromSeconds(this.LifeTime))
                .TakeUntilDisable(this)
                .Subscribe(_ => this.ReturnPool(this));
        }

        public void OnSpawned()
        {
            ParticleSystem.Clear();
            ParticleSystem.Play();

            Init();
        }
    }
}