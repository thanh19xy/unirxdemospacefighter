using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public class AudioPlayer : MonoBehaviour
    {
        private AudioSource audioSource;

        private AudioSource AudioSource =>
            audioSource ? audioSource : audioSource = Camera.main.GetComponent<AudioSource>();

        public void Play(AudioClip clip, float volume)
        {
            AudioSource.PlayOneShot(clip, volume);
        }
    }
}