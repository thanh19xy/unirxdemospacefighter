using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public struct DamageMessage
    {
        public float Damage { get; }

        public Vector3 Direction { get; }

        public DamageMessage(float damage, Vector3 direction)
        {
            Damage = damage;
            Direction = direction;
        }
    }
}