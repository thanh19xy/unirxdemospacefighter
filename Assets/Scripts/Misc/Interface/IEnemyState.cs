using System;
using UniRx;

namespace UniRxDemo.SpaceFighter
{
    public interface IEnemyState
    {
        void EnterState();

        IObservable<Unit> OnEnterStateAsObservable();

        void ExitState();

        IObservable<Unit> OnExitStateAsObservable();
    }
}