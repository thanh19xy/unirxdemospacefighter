namespace UniRxDemo.SpaceFighter
{
    public interface IReceiveDamage
    {
        void TakeDamage(DamageMessage damageMessage);
    }
}