using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public class Bullet : MonoBehaviour, IPoolable<Bullet>
    {
        private float Speed { get; set; }
        private float LifeTime { get; set; }

        private MeshRenderer meshRenderer;

        private MeshRenderer MeshRenderer =>
            meshRenderer ? meshRenderer : meshRenderer = this.GetComponent<MeshRenderer>();

        private Vector3 MoveDirection => this.transform.right;

        [SerializeField] private float damage;

        private float Damage => damage;

        private void Start()
        {
            this.OnTriggerEnterAsObservable()
                .Subscribe(this.ApplyDamage);
        }

        private void Init()
        {
            Observable.EveryUpdate()
                .TakeUntilDisable(this)
                .Subscribe(_ => Move());

            Observable.Timer(TimeSpan.FromSeconds(LifeTime))
                .TakeUntilDisable(this)
                .Subscribe(_ => this.ReturnPool(this));
        }

        public void OnSpawned(SettingBullet settingBullet)
        {
            this.Speed = settingBullet.Speed;
            this.LifeTime = settingBullet.LifeTime;
            this.MeshRenderer.material = settingBullet.Material;
            this.gameObject.layer = settingBullet.LayerMask;

            this.Init();
        }

        private void Move()
        {
            transform.position -= transform.right * Speed * Time.deltaTime;
        }

        private void ApplyDamage(Collider collider)
        {
            var objectReceive = collider.GetComponent<IReceiveDamage>();

            if (objectReceive == null) return;
            objectReceive.TakeDamage(new DamageMessage(Damage, MoveDirection));
            this.ReturnPool(this);
        }
    }
}