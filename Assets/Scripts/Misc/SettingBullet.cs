using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public struct SettingBullet
    {
        public float Speed { get; }
        
        public float LifeTime { get; }
        
        public Material Material { get; }
        
        public LayerMask LayerMask { get; }

        public SettingBullet(float speed, float lifeTime, Material material, LayerMask layerMask)
        {
            Speed = speed;
            LifeTime = lifeTime;
            Material = material;
            LayerMask = layerMask;
        }
    }
}