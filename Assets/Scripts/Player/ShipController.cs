using System;
using UniRx;
using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public class ShipController : MonoBehaviour
    {
        [SerializeField] private ShipModel shipModel;

        private ShipModel ShipModel => shipModel;

        private Subject<DamageMessage> OnTakeDamageSubject { get; } = new Subject<DamageMessage>();

        public bool IsDied => ShipModel.Health.Value <= 0;

        public MeshRenderer MeshRenderer => ShipModel.MeshRenderer;

        private Rigidbody Rigidbody => ShipModel.Rigidbody;

        public Vector3 LookDir => -Rigidbody.transform.right;

        public Quaternion Rotation
        {
            get { return this.Rigidbody.rotation; }
            set { this.Rigidbody.rotation = value; }
        }

        public Vector3 Position
        {
            get { return this.Rigidbody.position; }
            set { this.Rigidbody.position = value; }
        }

        public void AddForce(Vector3 force)
        {
            Rigidbody.AddForce(force);
        }

        public Vector3 Velocity => Rigidbody.velocity;

        public Vector3 RightDir => Rigidbody.transform.up;

        public IObservable<float> OnHealthChangedAsObservable => this.ShipModel.Health.AsObservable();

        public IObservable<DamageMessage> OnTakeDamageAsObservable => this.OnTakeDamageSubject;

        public IObservable<Unit> OnDiedAsObservable =>
            OnHealthChangedAsObservable.Where(_ => IsDied).AsUnitObservable();

        private void Start()
        {
            this.OnTakeDamageAsObservable
                .TakeWhile(_ => !this.IsDied)
                .RepeatUntilDestroy(this)
                .Subscribe(dam => ShipModel.Health.Value = Mathf.Max(0.0f, ShipModel.Health.Value - dam.Damage));
        }

        public void TakeDamage(DamageMessage damageMessage)
        {
            this.OnTakeDamageSubject.OnNext(damageMessage);
        }

        public void AddTorque(float value)
        {
            Rigidbody.AddTorque(Vector3.forward * value);
        }

        public void OnRespawn(float health)
        {
            this.ShipModel.Health.Value = health;
        }
    }
}