using UniRx;
using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public class PlayerDirectionHandler : MonoBehaviour
    {
        private ShipController ShipController { get; set; }
        
        private Camera Camera { get; set; }

        private void Awake()
        {
            this.ShipController = this.GetComponent<ShipController>();
            this.Camera = Camera.main;
        }

        private void Start()
        {
            Observable.EveryUpdate()
                .Subscribe(_ =>
                {
                    var mouseRay = Camera.ScreenPointToRay(Input.mousePosition);

                    var mousePos = mouseRay.origin;
                    mousePos.z = 0;

                    var goalDir = mousePos - ShipController.Position;
                    goalDir.z = 0;
                    goalDir.Normalize();

                    ShipController.Rotation = Quaternion.LookRotation(goalDir) * Quaternion.AngleAxis(90, Vector3.up);
                }).AddTo(this);
        }
    }
}