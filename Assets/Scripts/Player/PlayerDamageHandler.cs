using System;
using UnityEngine;
using UniRx;
using UnityEngine.SceneManagement;

namespace UniRxDemo.SpaceFighter
{
    public class PlayerDamageHandler : MonoBehaviour, IPoolable<Explosion>, IReceiveDamage
    {
        private ShipController ShipController { get; set; }

        [SerializeField] private Settings settings;

        private Settings SettingsHandler => this.settings;

        private AudioPlayer AudioPlayer { get; set; }

        private void Awake()
        {
            this.ShipController = this.GetComponent<ShipController>();
            this.AudioPlayer = this.GetComponent<AudioPlayer>();
        }

        private void Start()
        {
            this.ShipController.OnDiedAsObservable
                .Subscribe(_ => Died());
        }

        public void TakeDamage(DamageMessage damageMessage)
        {
            this.ShipController.TakeDamage(damageMessage);
            ShipController.AddForce(-damageMessage.Direction * SettingsHandler.HitForce);
            AudioPlayer.Play(SettingsHandler.HitSound, SettingsHandler.HitSoundVolume);
        }

        private void Died()
        {
            this.RentAsync()
                .Subscribe(explosion =>
                {
                    explosion.transform.position = ShipController.Position;
                    explosion.OnSpawned();
                });
            this.AudioPlayer.Play(SettingsHandler.DeathSound, SettingsHandler.HitSoundVolume);

            this.RestartGame();
            gameObject.SetActive(false);
        }

        private void RestartGame()
        {
            Observable.Timer(TimeSpan.FromSeconds(SettingsHandler.RestartDelay))
                .Subscribe(_ => SceneManager.LoadScene(SceneManager.GetActiveScene().name));
        }

        [Serializable]
        public class Settings
        {
            [SerializeField] private float hitForce;

            public float HitForce => hitForce;

            [SerializeField] private AudioClip hitSound;

            public AudioClip HitSound => hitSound;

            [SerializeField] private AudioClip deathSound;

            public AudioClip DeathSound => deathSound;

            [SerializeField] private float hitSoundVolume = 1.0f;

            public float HitSoundVolume => hitSoundVolume;

            [SerializeField] private float restartDelay = 3f;

            public float RestartDelay => restartDelay;
        }
    }
}