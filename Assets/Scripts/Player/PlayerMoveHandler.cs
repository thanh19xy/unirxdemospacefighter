using System;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public class PlayerMoveHandler : MonoBehaviour, IInputHandler
    {
        private readonly Dictionary<KeyCode, Vector3> DELEGATE_MAP = new Dictionary<KeyCode, Vector3>()
        {
            {KeyCode.W, Vector3.up},
            {KeyCode.A, Vector3.left},
            {KeyCode.S, Vector3.down},
            {KeyCode.D, Vector3.right},
            {KeyCode.UpArrow, Vector3.up},
            {KeyCode.LeftArrow, Vector3.left},
            {KeyCode.DownArrow, Vector3.down},
            {KeyCode.RightArrow, Vector3.right},
        };

        [SerializeField] private Settings settings;
        private Settings PlayerSettings => this.settings;

        private ShipController ship;
        private ShipController Ship => this.ship ? ship : ship = this.GetComponent<ShipController>();

        private LevelBoundary LevelBoundary { get; set; }

        private void Start()
        {
            this.LevelBoundary = new LevelBoundary();

            foreach (var key in DELEGATE_MAP)
            {
                this.OnInputKeyAsObservable(key.Key)
                    .TakeUntilDisable(this)
                    .Select(_ => key.Value)
                    .Subscribe(MoveDirection);
            }

            Observable.EveryUpdate()
                .TakeUntilDisable(this)
                .Subscribe(_ => KeepPlayerOnScreen());
        }

        private void MoveDirection(Vector3 direction)
        {
            Ship.AddForce(direction * PlayerSettings.MoveSpeed);
        }

        private void KeepPlayerOnScreen()
        {
            var extentLeft = (LevelBoundary.Left + PlayerSettings.BoundaryBuffer) - Ship.Position.x;
            var extentRight = Ship.Position.x - (LevelBoundary.Right - PlayerSettings.BoundaryBuffer);

            if (extentLeft > 0)
            {
                Ship.AddForce(
                    Vector3.right * PlayerSettings.BoundaryAdjustForce * extentLeft);
            }
            else if (extentRight > 0)
            {
                Ship.AddForce(
                    Vector3.left * PlayerSettings.BoundaryAdjustForce * extentRight);
            }

            var extentTop = Ship.Position.y - (LevelBoundary.Top - PlayerSettings.BoundaryBuffer);
            var extentBottom = (LevelBoundary.Bottom + PlayerSettings.BoundaryBuffer) - Ship.Position.y;

            if (extentTop > 0)
            {
                Ship.AddForce(
                    Vector3.down * PlayerSettings.BoundaryAdjustForce * extentTop);
            }
            else if (extentBottom > 0)
            {
                Ship.AddForce(
                    Vector3.up * PlayerSettings.BoundaryAdjustForce * extentBottom);
            }
        }

        [Serializable]
        public class Settings
        {
            [SerializeField] private float boundaryBuffer;
            public float BoundaryBuffer => this.boundaryBuffer;

            [SerializeField] private float boundaryAdjustForce;
            public float BoundaryAdjustForce => this.boundaryAdjustForce;

            [SerializeField] private float moveSpeed;
            public float MoveSpeed => this.moveSpeed;

            [SerializeField] private float slowDownSpeed;
            public float SlowDownSpeed => this.slowDownSpeed;
        }
    }
}