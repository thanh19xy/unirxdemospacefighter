using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.Serialization;

namespace UniRxDemo.SpaceFighter
{
    public class PlayerShootHandler : MonoBehaviour, IInputHandler, IPoolable<Bullet>
    {
        [SerializeField] private Setting setting;

        private Setting SettingShoot => setting;

        private ShipController ShipController { get; set; }

        private AudioPlayer AudioPlayer { get; set; }

        private void Awake()
        {
            this.ShipController = this.GetComponent<ShipController>();
            this.AudioPlayer = this.GetComponent<AudioPlayer>();
        }

        private void Start()
        {
            var mouseDownStream = this.UpdateAsObservable().Where(_ => Input.GetMouseButtonDown(0));
            var mouseUpStream = this.UpdateAsObservable().Where(_ => Input.GetMouseButtonUp(0));
            var intervalStream =
                this.UpdateAsObservable().ThrottleFirst(TimeSpan.FromSeconds(SettingShoot.MaxShootInterval));

            var shootStream = mouseDownStream
                .SelectMany(_ => intervalStream)
                .TakeUntil(mouseUpStream) // stop shooting when mouseUp emit
                .RepeatUntilDestroy(this) // repeat the process
                .Subscribe(_ => Fire());
        }

        private void Fire()
        {
            this.RentAsync()
                .Subscribe(bullet =>
                {
                    bullet.OnSpawned(this.SettingBullet());

                    bullet.transform.position =
                        ShipController.Position + ShipController.LookDir * SettingShoot.BulletOffsetDistance;
                    bullet.transform.rotation = ShipController.Rotation;
                });

            this.AudioPlayer.Play(SettingShoot.Laser, SettingShoot.LaserVolume);
        }

        private SettingBullet SettingBullet()
        {
            return new SettingBullet(SettingShoot.BulletSpeed,
                SettingShoot.BulletLifetime,
                SettingShoot.MaterialBullet,
                this.gameObject.layer);
        }

        [Serializable]
        public class Setting
        {
            [SerializeField] private AudioClip laser;

            public AudioClip Laser => laser;

            [SerializeField] private float laserVolume = 1.0f;

            public float LaserVolume => laserVolume;

            [SerializeField] private float bulletLifetime;

            public float BulletLifetime => bulletLifetime;

            [SerializeField] private float bulletSpeed;

            public float BulletSpeed => bulletSpeed;

            [SerializeField] private float maxShootInterval;

            public float MaxShootInterval => maxShootInterval;

            [SerializeField] private float bulletOffsetDistance;

            public float BulletOffsetDistance => bulletOffsetDistance;

            [SerializeField] private Material materialBullet;

            public Material MaterialBullet => materialBullet;
        }
    }
}