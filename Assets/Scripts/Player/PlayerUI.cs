using UnityEngine;
using UniRx;
using UnityEngine.UI;

namespace UniRxDemo.SpaceFighter
{
    public class PlayerUI : MonoBehaviour
    {
        private const string BEST_KILL = "BestKill";

        [SerializeField] private ShipController player;

        private ShipController Player => player;

        [SerializeField] private EnemySpawner enemySpawner;

        private EnemySpawner EnemySpawner => enemySpawner;

        [SerializeField] private Text killCountText;

        private Text KillCountText => killCountText;

        [SerializeField] private Text bestKillText;

        private Text BestKillText => bestKillText;

        [SerializeField] private Slider healthBar;

        private Slider HealthBar => healthBar;

        private void Start()
        {
            Player.OnHealthChangedAsObservable
                .Subscribe(health => HealthBar.value = health);

            EnemySpawner.KillCountAsObservable()
                .Select(count => $"Kill Count: {count}")
                .SubscribeToText(KillCountText);

            //Check best score
            EnemySpawner.KillCountAsObservable()
                .Scan(PlayerPrefs.GetInt(BEST_KILL), Mathf.Max)
                .Do(this.SaveScore)
                .Select(score => $"Best Kill: {score}")
                .SubscribeToText(BestKillText);
        }

        private void SaveScore(int score)
        {
            PlayerPrefs.SetInt(BEST_KILL, score);
            PlayerPrefs.Save();
        }
    }
}