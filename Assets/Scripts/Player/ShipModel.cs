using System;
using System.Runtime.CompilerServices;
using UniRx;
using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    [Serializable]
    public class ShipModel
    {
        public FloatReactiveProperty Health { get; } = new FloatReactiveProperty(100);
        
        [SerializeField] private MeshRenderer meshRenderer;
        
        public MeshRenderer MeshRenderer => meshRenderer;

        [SerializeField] private Rigidbody rigidbody;
        
        public Rigidbody Rigidbody => rigidbody;
    }
}