using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public class BulletPoolManager : ObjectPoolManager<ObjectPool<Bullet>, Bullet>
    {
        protected override ObjectPool<Bullet> ObjectPool { get; set; }

        protected override void Awake()
        {
            base.Awake();
            this.ObjectPool = new ObjectPool<Bullet>(ObjectType, this.transform);
        }
    }
}