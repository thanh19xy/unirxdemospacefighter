using UniRx;

namespace UniRxDemo.SpaceFighter
{
    public class EnemyPoolManager : ObjectPoolManager<ObjectPool<EnemyShipController>, EnemyShipController>
    {
        protected override ObjectPool<EnemyShipController> ObjectPool { get; set; }

        protected override void Awake()
        {
            base.Awake();

            this.ObjectPool = new ObjectPool<EnemyShipController>(ObjectType, this.transform);

            this.PreloadAsync(5, 1).Subscribe();
        }
    }
}