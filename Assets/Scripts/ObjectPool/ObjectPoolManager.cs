using System;
using UniRx;
using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public abstract class ObjectPoolManager<TPool, TObjectType> : MonoBehaviour where TPool : ObjectPool<TObjectType>
        where TObjectType : Component
    {
        private static ObjectPoolManager<TPool, TObjectType> instance = null;

        public static ObjectPoolManager<TPool, TObjectType> Instance => instance;

        [SerializeField] private TObjectType objectType;

        public TObjectType ObjectType => objectType;

        protected abstract TPool ObjectPool { get; set; }

        protected virtual void Awake()
        {
            instance = this;
        }

        public IObservable<TObjectType> RentAsync()
        {
            return ObjectPool.RentAsync();
        }

        public void Return(TObjectType objectType)
        {
            ObjectPool.Return(objectType);
        }

        public IObservable<Unit> PreloadAsync(int preloadCount, int threshold)
        {
            return ObjectPool.PreloadAsync(preloadCount, threshold);
        }
    }
}