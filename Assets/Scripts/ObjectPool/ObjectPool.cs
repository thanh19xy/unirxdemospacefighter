using System;
using UniRx;
using UniRx.Toolkit;
using UnityEngine;
using Object = UnityEngine.Object;

namespace UniRxDemo.SpaceFighter
{
    public class ObjectPool<T> : AsyncObjectPool<T> where T : Component
    {
        private readonly T objectPrefab;
        private readonly Transform parentTransform;

        public ObjectPool(T objectPrefab, Transform parentTransform)
        {
            this.objectPrefab = objectPrefab;
            this.parentTransform = parentTransform;
        }

        protected override IObservable<T> CreateInstanceAsync()
        {
            var obj = Object.Instantiate(objectPrefab, parentTransform);
            return Observable.Return(obj);
        }

        protected override void OnBeforeReturn(T instance)
        {
            base.OnBeforeReturn(instance);
            instance.transform.localPosition = Vector3.zero;
        }
    }

    public class BulletPool : ObjectPool<Bullet>
    {
        public BulletPool(Bullet objectPrefab, Transform parentTransform) : base(objectPrefab, parentTransform)
        {
        }
    }
}