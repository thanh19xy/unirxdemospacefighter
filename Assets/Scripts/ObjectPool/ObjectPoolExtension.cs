using System;
using UniRx;
using UnityEngine;

namespace UniRxDemo.SpaceFighter
{
    public interface IPoolable<TObjectType>
    {
    }

    public static class ObjectPoolExtension
    {
        public static IObservable<T> RentAsync<T>(this IPoolable<T> poolable) where T : Component
        {
            return ObjectPoolManager<ObjectPool<T>, T>.Instance.RentAsync();
        }

        public static void ReturnPool<T>(this IPoolable<T> poolable, T objectType) where T : Component
        {
            ObjectPoolManager<ObjectPool<T>, T>.Instance.Return(objectType);
        }
    }
}