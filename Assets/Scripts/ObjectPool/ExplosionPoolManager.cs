using UniRx;

namespace UniRxDemo.SpaceFighter
{
    public class ExplosionPoolManager : ObjectPoolManager<ObjectPool<Explosion>, Explosion>
    {
        protected override ObjectPool<Explosion> ObjectPool { get; set; }

        protected override void Awake()
        {
            base.Awake();

            this.ObjectPool = new ObjectPool<Explosion>(ObjectType, this.transform);

            this.PreloadAsync(5, 1).Subscribe();
        }
    }
}